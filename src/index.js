import keyboard from './components/keyboard/index.vue'
keyboard.install = Vue => Vue.component('Keyboard',keyboard)

if(typeof window != 'undefined' && window.Vue){
    window.Vue.use(keyboard)
}
export default keyboard