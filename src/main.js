import Vue from 'vue'
import App from './App.vue'
import keyboard from '../lib/password-virtual-keyboard-vue.common.js'
import '../lib/password-virtual-keyboard-vue.css'
Vue.config.productionTip = false
Vue.use(keyboard)
new Vue({
  render: h => h(App),
}).$mount('#app')
