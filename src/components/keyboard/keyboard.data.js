const keyCodes = {
    number:[
        {
            code:'1'
        },
        {
            code:'2'
        },
        {
            code:'3'
        },
        {
            code:'4'
        },
        {
            code:'5'
        },
        {
            code:'6'
        },
        {
            code:'7'
        },
        {
            code:'8'
        },
        {
            code:'9'
        },
        {
            code:'0'
        }
    ],
    symbols:[
        {
            code:'~'
        },
        {
            code:'!'
        },
        {
            code:'@'
        },
        {
            code:'#'
        },
        {
            code:'$'
        },
        {
            code:'%'
        },
        {
            code:'^'
        },
        {
            code:'&'
        },
        {
            code:'*'
        },
        {
            code:'('
        },
        {
            code:')'
        },
        {
            code:'_'
        },
        {
            code:'+'
        },
        {
            code:'-'
        },
        {
            code:'/'
        },
        {
            code:'\\'
        },
        {
            code:'<'
        },
        {
            code:'>'
        },
        {
            code:'['
        },
        {
            code:']'
        },
        {
            code:'{'
        },
        {
            code:'}'
        },
        {
            code:'?'
        },
        {
            code:','
        },
        {
            code:'.'
        },
        {
            code:';'
        },
        {
            code:'\''
        },
        {
            code:':'
        },
        {
            code:'"'
        },
        {
            code:'|'
        }
    ],
    lowercase:[
        {
            code:'a'
        },
        {
            code:'b'
        },
        {
            code:'c'
        },
        {
            code:'d'
        },
        {
            code:'e'
        },
        {
            code:'f'
        },
        {
            code:'g'
        },
        {
            code:'h'
        },
        {
            code:'i'
        },
        {
            code:'j'
        },
        {
            code:'k'
        },
        {
            code:'l'
        },
        {
            code:'m'
        },
        {
            code:'n'
        },
        {
            code:'o'
        },
        {
            code:'p'
        },
        {
            code:'q'
        },
        {
            code:'r'
        },
        {
            code:'s'
        },
        {
            code:'t'
        },
        {
            code:'u'
        },
        {
            code:'v'
        },
        {
            code:'w'
        },
        {
            code:'x'
        },
        {
            code:'y'
        },
        {
            code:'z'
        }
    ],
    capital:[
        {
            code:'A'
        },
        {
            code:'B'
        },
        {
            code:'C'
        },
        {
            code:'D'
        },
        {
            code:'E'
        },
        {
            code:'F'
        },
        {
            code:'G'
        },
        {
            code:'H'
        },
        {
            code:'I'
        },
        {
            code:'J'
        },
        {
            code:'K'
        },
        {
            code:'L'
        },
        {
            code:'M'
        },
        {
            code:'N'
        },
        {
            code:'O'
        },
        {
            code:'P'
        },
        {
            code:'Q'
        },
        {
            code:'R'
        },
        {
            code:'S'
        },
        {
            code:'T'
        },
        {
            code:'U'
        },
        {
            code:'V'
        },
        {
            code:'W'
        },
        {
            code:'X'
        },
        {
            code:'Y'
        },
        {
            code:'Z'
        }
    ]
}

export {
    keyCodes
}