import {keyCodes} from './keyboard.data'
export default {
    name:'keyboard',
    data(){
        return {
            inputList:[],
            keyboardTypeList:[
                {
                    title:'数字',
                    id:'number'
                },
                {
                    title:'特殊符号',
                    id:'symbols'
                },
                {
                    title:'字母',
                    id:'lowercase'
                }
            ],
            typeIndex:0,
            keyCodes,
            keyType:'number',
            keyCodeArr:[],
            isEnter:false,
            isShowKeyboard:false,
            isClear:'password',
            cursorIndex:-1
        }
    },
    props:{
        inputNum:{
            type:Number,
            default:6
        },
        showClear:{
            type:Boolean,
            default:false
        },
        isDislocate:{
            type:Boolean,
            default:false
        }
    },
    computed:{
        chackKeyType:({keyType})=>{
            let res = ''
            let typeArray = ['lowercase','symbols','capital']
            res = typeArray.includes(keyType) ? 'letters-code' : ''
            return res
        }
    },
    mounted() {
        this.init()
        let body = document.querySelector('body')
        body.addEventListener('click',()=>{
            this.isShowKeyboard = false
            this.cursorIndex = -1
        })
    },
    methods: {
        init(){
            for (let index = 0; index < this.inputNum; index++) {
                this.inputList.push({
                    id:index+1,
                    value:''
                })
            }
            if(this.isDislocate){
                this.shuffleKeyCodes()
            }
        },
        shuffleKeyCodes(){
            let temp = this.keyCodes[this.keyType]
            this.keyCodes[this.keyType] = [...this.shuffle(temp)]
        },
        chackType(index,keyType){
            this.typeIndex = index
            this.keyType = keyType
            if(this.isDislocate){
                this.shuffleKeyCodes()
            }
        },
        chackCapital(){
            this.keyType = this.keyType == 'lowercase' ? 'capital' :'lowercase'
        },
        getKeyboardCode(code){
            this.isEnter = false
            if(this.keyCodeArr.length < this.inputNum){
                this.cursorIndex++
                this.keyCodeArr.push(code)
                this.keyCodeArr.forEach((ele,index) => {
                    this.inputList[index].value = ele
                });
            }else{
                console.log('满了')
                return
            }
            this.$emit('changeInputValue',code)
        },
        backSpaceEvent(){
            this.isEnter = false
            if(this.keyCodeArr.length != 0){
                this.cursorIndex--
                this.inputList[this.keyCodeArr.length-1].value = ''
                this.keyCodeArr.splice((this.keyCodeArr.length-1),1)
            }else{
                console.log('空了')
                return
            }
        },
        emptyInputList(){
            this.keyCodeArr = []
            this.cursorIndex = 0
            this.inputList.forEach(val=>val.value ='')
            this.isEnter = false
        },
        enterEvent(){
            let res = this.keyCodeArr.join('')
            if(res.length < this.inputNum){
                this.isEnter = true
            }else{
                this.isEnter = false
                this.isShowKeyboard = false
                this.cursorIndex = -1
                this.$emit('complete',res)
            }
        },
        showClearEvent(){
            this.isClear = this.isClear === 'password' ? 'text' : 'password'
        },
        showKeyboard(){
            this.isShowKeyboard = true
            if(this.keyCodeArr.length != 0 && this.cursorIndex <= this.keyCodeArr.length){
                this.cursorIndex = this.keyCodeArr.length
            }else{
                this.cursorIndex = 0
            }
            if(this.isDislocate){
                this.shuffleKeyCodes()
            }
        },
        shuffle(arr){
            let m = arr.length;
            while (m){
                let index = Math.floor(Math.random() * m--);
                let cur = arr[m];
                arr[m] = arr[index];
                arr[index] = cur;
            }
            return arr;
        }
    },
}