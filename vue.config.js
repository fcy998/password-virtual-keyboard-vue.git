const path = require("path");
module.exports = {
  // 修改打包出口，在最外级目录打包出一个 index.js 文件，我们 import 默认会指向这个文件
  outputDir: path.resolve(__dirname, "./dist/keyboard"),
  configureWebpack: {
    // webpack 配置
    output: {
      // 输出重构  打包编译后的 文件名称  【模块名称.版本号.时间戳】
      filename: `keyboard.min.js`,
      library: "keyboard", // 指定的就是你使用require时的模块名
      libraryTarget: "umd",
    },
  },
  chainWebpack: (config) => {
    config.module
      .rule("images")
      .use("url-loader")
      .loader("url-loader")
      .tap((options) =>
        Object.assign(options, { limit: 20000, esModule: false })
      );
  },
  productionSourceMap: false,
};
