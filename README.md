# password-virtual-keyboard-vue

```
本组件是在闲暇之余随便写的一个简单的密码模拟键盘，支持数字、特殊字符、字母大小写，同时目前还未正式完善，因此在有时间的情况下会进行补充完善
```

```
v1.1.x 版本说明
  1.1.1 *修改组件发布方式，修改组件的引入方式

v1.0.x 版本说明

  1.0.9 *修复文件引入问题
 
  1.0.8 *新增键盘顺序打乱功能（可选），并在数字键盘中移除“ * ”和“ # ”两个符号
  
  1.0.7 *新增明/密文显示按钮，新增输入光标显示

  1.0.6 *修复列表样式问题

  1.0.5 *修复部分bug，对样式细节进行了完善，添加自定义密码长度功能

  1.0.4 *简单的进行键盘模拟，支持changeInputValue(触发键盘按键时)与complete(点击完成按钮时)回调
```

#### 组件技术栈

```
JavaScript、Vuejs
```

#### 使用教程

##### 使用终端安装

```
npm install password-virtual-keyboard-vue  
```

##### 在vue/main.js中进行组件注册

```
//引入主文件
import keyboard from 'password-virtual-keyboard-vue'
//引入组件样式文件
import 'password-virtual-keyboard-vue/lib/password-virtual-keyboard-vue.css'
Vue.use(keyboard)
```

##### 在页面中使用

```
<template>
  <div id="app">
    <Keyboard 
    @complete="getResult"
    @changeInputValue="changeValue" 
    :inputNum="6" 
    :showClear="true"
    :isDislocate="true">
    </Keyboard>
  </div>
</template>

<script>
export default {
  name: 'App',
  methods:{
    getResult(e){
      console.log(e)
    },
    changeValue(e){
      console.log(e)
    }
  }
}
</script>
```

##### 参数说明

```
@complete | Function | 点击完成按钮触发
@changeInputValue  |  Function  |  点击按键输入时触发
:inputNum | Number  |  可定义密码长度，默认为6个
:showClear | Boolean | 可设置是否显示明/密文按钮，默认为false
:isDislocate | Boolean | 可设置是否打乱密码键盘，默认为false
```
